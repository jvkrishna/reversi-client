/**
 * Created by revanth, Vamsi Krishna J
 * on 11/11/17.
 */

/** Game Script **/
var rows =8, cols =8, black =0, white =1, empty =-1, current_player = black, turn=false;
var board = []



var directions = {
    NorthWest : -11,
    North: -10,
    NorthEast: -9,
    West: -1,
    East: 1,
    SouthWest: 9,
    South: 10,
    SouthEast: 11
}

/**
 * Initialize the board.
 */
var initBoard = () => {
    populateBoard();
}

function populateBoard() {
    $("table#reversi-table").find("td").removeClass("disabled enabled")
    $("table#reversi-table").find("div#circle").remove()
    let blackCount=0, whiteCount =0;
    for(let i=1; i <= rows;i++) {
        for(let j=1; j<= cols; j++ ) {
            let index = 10*i+j
            if(board[index] == white || board[index] == black) {
                let stone;
                if(board[index] == white) {
                    stone = 'white'
                    whiteCount++
                } else if (board[index] == black) {
                    stone = 'black'
                    blackCount++
                }
                $("td#"+index).html(`<div id='circle' style='background: ${stone}'></div>`)
            }
            $("td#"+index).addClass(turn && isValidMove(index,current_player) ? "enabled":"disabled")


        }

        if((turn && current_player == black) || (!turn && current_player !=black)) {
            $("#arrow-black").show();
            $("#arrow-white").hide();
        }
        else{
            $("#arrow-black").hide();
            $("#arrow-white").show();
        }

        $("#black_count").text(blackCount);
        $("#white_count").text(whiteCount);
    }
}

var isValidIndex = (index) =>  index >= 0 && index < 100 && board[index] !== undefined


/**
 * Check is it a valid move
 * @param position
 * @param player
 */
var isValidMove = (position, player) => {
    if(isValidIndex(position) && board[position] == empty) {
        for(let direction in directions) {
            let newPos = position+directions[direction]
            if(isValidIndex(newPos) && board[newPos] == !player) {
                do {
                    newPos += directions[direction]
                    if(!isValidIndex(newPos) || board[newPos] == empty)
                        break
                    if(board[newPos] == player)
                        return true
                } while(true)
            }
        }

    }
    return false;
}

function add_circle(ids,currentplayer)
{
    for(var i=0;i<ids.length;i++)
    {
        element = document.getElementById(ids[i]);


        if(currentplayer == black)
        {
            element.innerHTML = "<div id='circle' style='background: black'></div>";
        }
        else
            element.innerHTML = "<div id='circle' style='background: white'></div>";

    }
}

/**
 *  Get valid positions for a player.
 * @param player
 * @returns {Array}
 */
var getValidPositions = (player) => {
    let validPositions = [];
    for(let i=1; i <= rows;i++) {
        for(let j=1; j<= cols; j++ ) {
            let index = i*10+j
            if (isValidMove(index, player))

                validPositions.push(index)
        }
    }

    return validPositions;
}

/**
 * Update the current board state.
 * @param position
 */
var updateBoard = (position, player) => {
    board[position] = player
    for(let direction in directions) {
        let newPos = position+directions[direction]
        if(isValidIndex(newPos) && board[newPos] == !player) {
            do {
                newPos += directions[direction]
                if(!isValidIndex(newPos) || board[newPos] == empty)
                    break
                if(board[newPos] == player) {
                    let tmpPos = position+directions[direction]
                    while(tmpPos != newPos) {
                        board[tmpPos] = player
                        tmpPos +=  directions[direction]
                    }
                    break
                }

            } while(true)
        }
    }
}


var generateReq = (action, pos, message) => {
    let req = {
        action: action,
        position:pos,
        message: message

    }
    return JSON.stringify(req)
}

var showMessage = (message, displayFooter, yesHandler, noHandler) => {

    $("#messageModal").on('show.bs.modal',(e) => {
       if(!displayFooter) $(".modal-footer").hide()
       else $(".modal-footer").show()
        $(".modal-body").html(message)
    })
    $("#messageModal").on('shown.bs.modal',(e) => {
        if(displayFooter) {
            if(typeof yesHandler == 'function') {
                $("#modal-yes").unbind('click')
                $("#modal-yes").click(yesHandler)
            }
            if(typeof  noHandler == 'function') {
                $("#modal-no").unbind('click')
                $("#modal-no").click(noHandler)

            }
        }
    })

    $("#messageModal").modal()
}

var setCookie = (cname,cval,exdays) => {
    let d = new Date()
    d.setTime(d.getTime() + (exdays*24*60*60*1000))
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cval + ";" + expires + ";path=/";
}

var getCookie = (cname) => {
    let name = cname+"="
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var randomString = (len) => {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < len; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}







var showDiv = (id) => {
    let divs  =['details', 'start-game-div','reversi']
    divs.forEach(div => {
        if(div == id)  $(`div#${div}`).show()
        else  $(`div#${div}`).hide()
    })
}

var playerName = getCookie('name')

if(!playerName) {
    showDiv('details')
} else {
    showDiv('start-game-div')
}


var ws, myTimeout, gameTimer, timer




var startTimer = (duration) => {
    timer = setInterval(() => {
        console.log(timer)
        $("#timer").html(`${duration} secs`)
        duration--
    }, 1000)
    gameTimer = setTimeout(() => {
        clearInterval(timer)
        sendData(generateReq('TIMEOUT',-100,'Opponent game timedout.'))
        showMessage('Game timeout.')
        endGameHandler()
    }, duration*1000)

}

var clearTimer = () => {
    $("#timer").html('')
    clearInterval(timer)
    clearTimeout(gameTimer)
}







var handleMessage =(message) => {
    let res = JSON.parse(message.data)
    console.log(res)
    switch(res.action) {
        case "START":
            showDiv('reversi')
            let oName = res.message
            setCookie('oname', oName,1)
            clearTimeout(myTimeout)
            current_player = res.color;
            board = res.board
            turn = res.turn
            initBoard()
            //display names
            $("#player_b").html(current_player == black ? getCookie('name'): getCookie('oname'))
            $("#player_w").html(current_player == white ? getCookie('name'): getCookie('oname'))
            if(oName) showMessage(`You are playing against ${oName}`)
            if(turn)  startTimer(120)
            break

        case "PLAY":
            board = res.board
            turn = res.turn
            populateBoard()
            //Check for NO_MOVE condition.
            if(getValidPositions(current_player).length < 1) {
                turn = false
                showMessage('No valid move exists for you.')
                sendData(generateReq('NO_MOVE'))
            }
            if(turn) startTimer(120)
            break
        // No move exists for the opponent
        case "NO_MOVE":
            board = res.board
            turn = res.turn
            populateBoard()
            if(turn) startTimer(120)
            break

        case "END":
            showMessage(res.message)
            break

        case "RESTART":
            // $(document).on('click','#modal-yes', () => sendData(generateReq('NEW_GAME')))
            // $(document).on('click','#modal-no', () => sendData(generateReq('RESTART_DECLINE')))
            showMessage('Opponent wants to restart the game. Do you accept the request?',true,
                () => sendData(generateReq('NEW_GAME')),
                () => sendData(generateReq('RESTART_DECLINE')))
            break

        case "RESTART_DECLINE":
            showMessage(res.message)
            break

        case "TERMINATE":
            closeConnection(res.message)
            showDiv('start-game-div')
            break

    }
}

var handleOpen = (open) => {
    console.log('opened')
    let startReq = generateReq('START',null,getCookie('name'))
    sendData(startReq)
}
var handleClose  = (close) => {
   console.log("Session closed")
}

var handleError  = (error) => {
    console.log(`Websocket error:`,error)
    //TODO
}

var sendData = (data) => {
   ws.send(data)
}


var endGameHandler = () => {
    showDiv('start-game-div')
    sendData(generateReq('TERMINATE'))
    closeConnection('Game terminated.')
}

var restartGameHandler = () => {
    sendData(generateReq('RESTART'))
}

var closeConnection = (msg) => {
    showMessage(msg);
    ws.close();
    $('.loader').css("display","none");
    $('#start-game').css("display","block");
}




$("#start-game").click(e => {
    $("#start-game").hide()
    $(".loader").show()
    sessionId = randomString(20)
    setCookie('sessionId',sessionId, 1)
    ws = new WebSocket("ws://40.71.43.179:8080/reversi")
    ws.onopen = handleOpen
    ws.onmessage = handleMessage
    ws.onclose = handleClose
    ws.onerror = handleError
    myTimeout = setTimeout(()=>closeConnection('Sorry! We are having trouble finding an opponent for you!'), 10*1000)
})

$(window).bind('beforeunload',function(){
    if(ws != null && ws.readyState == ws.OPEN)
        return 'Are you sure you want to leave?'
    else
        return null

});

$(document).ready(function() {


    $("div#reversi").on('click','td.enabled', function (event) {
        clicked_element = event.target.id;
        add_circle([clicked_element],current_player);
        updateBoard(parseInt(clicked_element),current_player)
        turn = false
        populateBoard();
        let req = generateReq("PLAY",clicked_element,null)
        sendData(req)
        $("table#reversi-table").find("td").addClass("disabled");
        clearTimer()
    });

    $("#end-game").click(function () {
        // $(document).on('click','#modal-yes', endGameHandler)
        showMessage('Are you sure you want exit the game?',true,endGameHandler)
    });

    $("#restart-game").click(function () {
        // $(document).on('click','#modal-yes', restartGameHandler)
        showMessage('Are you sure you want restart the game?',true,restartGameHandler)
    });
    // On name submission -- Save to the cookie.
    $("#detail-form").on('submit',function () {
        let name = $("#name").val()
        setCookie('name',name,1)
        showDiv('start-game-div')
        return false
    })
});



