/**
 * Created by revanth on 12/4/17.
 */
// Cookies
var setCookie = (cname,cval,exdays) => {
    let d = new Date()
    d.setTime(d.getTime() + (exdays*24*60*60*1000))
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cval + ";" + expires + ";path=/";
}

var getCookie = (cname) => {
    let name = cname+"="
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var randomString = (len) => {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < len; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

var sessionId =  getCookie('sessionId')
console.log(sessionId)

var ws;

if(sessionId == null || sessionId == undefined || sessionId =='') {
    sessionId = randomString(20)
    setCookie('sessionId',sessionId, 1)
    ws = new WebSocket("ws://localhost:8080/reversi");

}
else
{
    // Send a message to the websocket and then the onmessage handler should take care of it
    var request = {action:"RESUME",position:"",message:""};
    ws = new WebSocket("ws://localhost:8080/reversi");
    ws.onopen = () => {
        ws.send(request.toString());
    };
}

var myTimeout;



$("#detail-form").on('submit',function () {

    $('#detail-form').css("display","none");
    $('.loader').css("display","block");

    var request = {action:"START",position:null,message:null};

    if(ws.readyState == ws.CLOSED)
        ws = new WebSocket("ws://localhost:8080/reversi");

    ws.send(JSON.stringify(request));

    myTimeout = setTimeout(closeConnection, 120*1000);
    return false;
});

$("#end-game").click(function () {
    var request = {action:"RESUME",position:"",message:""};
    ws.send(request.toString());
});

$("#restart-game").click(function () {
    var request = {action:"RESUME",position:"",message:""};
    ws.send(request.toString());
});

ws.onmessage = (message) => {
    let res = JSON.parse(message.data)
    console.log(res)
    if(res.action == "START") {
        clearTimeout(myTimeout);
        $(".well").load("index.html",function () {
            current_player = res.color;
            board = res.board
            initBoard();
            if(current_player == white)
            {
                $("td").addClass("disabled");
            }
        });
    }
    else if (res.action == "PLAY") {
        board = res.board
        //updateBoard(res.position, !current_player)
        populateBoard()

    } else if (res.action = "RESUME") {
        $(".well").load("index.html",function () {
            board = res.board
            current_player = res.color;
            updateBoard(res.position, !current_player)
            populateBoard()
        });
    }
    else if (res.action == "END") {
        //TODO
    }
}

var closeConnection = () => {
    alert("Sorry! We are having trouble finding an opponent for you!");
    ws.close();
    $('.loader').css("display","none");
    $('#detail-form').css("display","block");
}